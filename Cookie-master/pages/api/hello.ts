import type { NextApiRequest, NextApiResponse } from "next";

type Data = {
    name: string
}

//con esto ya obtenemos ls cookies del restapi
export default function handler( req: NextApiRequest, res: NextApiResponse<Data>) {
    console.log(req.cookies)

    res.status(200).json({
         name: 'john doe',
         ...req.cookies})
}