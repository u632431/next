import { Component, useEffect, useState } from 'react';
import type { AppContext, AppProps } from 'next/app'

import { ThemeProvider, Theme } from "@mui/material/styles";
import CssBaseline from '@mui/material/CssBaseline';
import { darkTheme, lightTheme, customTheme } from '../themes';
import Cookies from 'js-cookie';

function MyApp({ Component , pageProps}: AppProps ) {

  // console.log({theme})

  const [currentTheme, setCurrentTheme] = useState(lightTheme)

  useEffect(() => {
    
    const cookieTheme = Cookies.get('theme') || 'light';
    const selectedTheme = cookieTheme === 'light'
        ? lightTheme
        : (cookieTheme === 'dark')
          ? darkTheme
          : customTheme;
    
    setCurrentTheme( selectedTheme );
  }, [])
  

  return (
    <ThemeProvider theme={ currentTheme }>
      <CssBaseline />
      <Component {...pageProps} />
    </ThemeProvider>
  )
}
//método estático. Obtenemos datos iniciales antes de renderizar la página y pasarlos como props al componente

// MyApp.getInitialProps = async( appContext: AppContext ) => {

//   const { theme } = appContext.ctx.req ? ( appContext.ctx.req as any).cookies : { theme: 'light' }
  
//   const validThemes = ['light','dark','custom'];
//   // console.log('getInitialProps: ', cookies);

//   return {
//     theme: validThemes.includes( theme ) ? theme : 'dark',
//   }

// }


export default MyApp