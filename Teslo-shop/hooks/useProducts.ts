import useSWR, {SWRConfiguration} from 'swr';
import {IProduct}  from '../interfaces' 

//Esto se manda al _app.tsx 
/* const fetcher = (...args: [key: string]) => fetch(...args).then(res => res.json()) */

export const useProducts = (url: string, config: SWRConfiguration = {} ) =>{

    /* const { data, error } = useSWR<IProduct[]> (`/api/${ url }`, fetcher, config); */
    const { data, error } = useSWR<IProduct[]> (`/api/${ url }`, config);
    //stale while revalidate= hook para hacer peticiones

    return {
        products: data || [],
        isLoading: !error && !data,
        isError: error
    }
}