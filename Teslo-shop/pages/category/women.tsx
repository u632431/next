import { Typography } from "@mui/material";

import { ShopLayout } from "../../components/layouts";
import { ProductList } from "../../components/products";
import { useProducts } from "../../hooks";
import { FullSrcreenLoading } from "../../components/ui/FullSrcreenLoading";

const WomenPage = () =>{
   
    const {products, isLoading} = useProducts('/products?gender=women')

    return (
        <ShopLayout title={"Teslo-Shop - Home"} pageDescription={"Encuentra los mejores productos de Teslo para ellas"}>
            <Typography variant="h1" component='h1'>Mujer</Typography>
            <Typography variant="h2" sx={{ mb:1 }}>Productos para ellos</Typography>
            {isLoading
            ? <FullSrcreenLoading/>
            : <ProductList products={products} />
            }
            <ProductList
                products={products }
            />
        </ShopLayout>
    )
}

export default WomenPage