import { Typography } from "@mui/material";

import { ShopLayout } from "../../components/layouts";
import { ProductList } from "../../components/products";
import { useProducts } from "../../hooks";
import { FullSrcreenLoading } from "../../components/ui/FullSrcreenLoading";

const KidPage = () =>{
   
    const {products, isLoading} = useProducts('/products?gender=kid')

    return (
        <ShopLayout title={"Teslo-Shop - Kids"} pageDescription={"Encuentra los mejores productos de Teslo para niños"}>
            <Typography variant="h1" component='h1'>Niños</Typography>
            <Typography variant="h2" sx={{ mb:1 }}>Todos los productos para niños</Typography>
            {isLoading
            ? <FullSrcreenLoading/>
            : <ProductList products={products} />
            }
            <ProductList
                products={products }
            />
        </ShopLayout>
    )
}

export default KidPage