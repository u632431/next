import {ChangeEvent, useState, useMemo, FC, useContext} from 'react'
import { GetServerSideProps } from 'next';

import {capitalize, CardHeader, Grid, Card, CardContent, TextField, CardActions, Button, FormControl, FormLabel, RadioGroup, Radio, FormControlLabel, IconButton} from "@mui/material"
import SaveAsRoundedIcon from '@mui/icons-material/SaveAsRounded';
import DeleteForeverRoundedIcon from '@mui/icons-material/DeleteForeverRounded';

import { EntriesContext } from '../../context/entries';
import { Layout } from "../../components/Layouts"
import { Entry, EntryStatus } from "../../interfaces";
import { dbEntries } from '../../database';
import { dateFunctions } from '../../utils';



const validStatus: EntryStatus[] = ['pending', 'in-progress', 'finished']

interface Props{
    entry: Entry
}

export const EntryPage: FC<Props> = ( {entry}) =>{

    const {updateEntry} = useContext(EntriesContext)
    const [inputValue, setInputValue] = useState(entry.description);
    const [status, setStatus] = useState<EntryStatus>(entry.status);
    const [touched, setTouched] = useState(false)

    //lo que busca con esto es memorizar el valor que va a tener el inputValue y si ha sido tocado.
    //Como lo agrego aca, puedo sacarlo del helperText y el error
    const isNotValid =  useMemo(() => inputValue.length <= 0 && touched, [inputValue, touched])

    const onInputValueChanged= (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) =>{
        setInputValue(event.target.value)
    }

    const onStatusChanged = (event: ChangeEvent<HTMLInputElement>) =>{
        setStatus(event.target.value as EntryStatus)
    }

    const onSave =() =>{
        if( inputValue.trim().length === 0) return
        const updatedEntry: Entry ={
            ...entry,
            status,
            description: inputValue
        }
        updateEntry(updatedEntry, true)
    }

    return (
    <Layout title = {inputValue.substring(2,20) + '...'}>
        <Grid container
        justifyContent='center'
        sx={{marginTop: 2}}>
            <Grid item xs={12} sm={8} md={6}>
                    <Card>
                        <CardHeader
                            title={`Entrada:`}
                            subheader={`Created ${dateFunctions.getFormatDistanceToNow(entry.createdAt)}`}
                        />
                        <CardContent>
                            <TextField
                                sx={{ marginTop: 2, marginBotoom: 1 }}
                                fullWidth
                                placeholder="Nueva entrada"
                                autoFocus
                                multiline
                                label="Nueva entrada"
                                value={ inputValue}
                                onBlur={() => setTouched(true)}
                                onChange={onInputValueChanged}
                                helperText={isNotValid && 'ingrese un valor'}
                                error={isNotValid}
                                />
                                
                                <FormControl>
                                    <FormLabel>Estado:</FormLabel>
                                    <RadioGroup
                                         row
                                         value={status}
                                         onChange={onStatusChanged}
                                    >
                                        {
                                            validStatus.map( option =>(
                                                <FormControlLabel 
                                                    key={option}
                                                    value={option}
                                                    control={<Radio/>}
                                                    label={capitalize(option)} />
                                            ))
                                        }
                                    </RadioGroup>
                                </FormControl>
                        </CardContent>
                        <CardActions>
                            <Button
                                startIcon={<SaveAsRoundedIcon/>}
                                variant="contained"
                                fullWidth
                                onClick={ onSave}
                                disabled={ inputValue.length <= 0}
                            >
                                Save
                            </Button>
                        </CardActions>
                    </Card>
            </Grid>
        </Grid>
        <IconButton sx={{
            position:'fixed',
            bottom: 30,
            right: 30,
            background: 'red'
        }}>
                <DeleteForeverRoundedIcon/>       
        </IconButton>
    </Layout>)
}



//Solo se debe usar cuando la pagina tiene que renderizarse cuando el usuario hace la petición
//las props que se le envian al componente, las mano antes de la declaracion del entryPage
export const getServerSideProps: GetServerSideProps = async ({params}) =>{
    
const {id} = params as {id: string}

 const entry = await dbEntries.getEntryById(id)

    if (!entry){
        return {
            redirect: {
                destination: '/',
                permanent: false
            }
        }
    }
    return {
        props:{
            entry
        }
    }
}


export default EntryPage