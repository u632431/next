import '../styles/globals.css';
import type { AppProps } from 'next/app';
import { SnackbarProvider } from 'notistack';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { UIProvider } from '../context/ui';
import { EntriesProvider } from '../context/entries';
import { lightTheme, darkTheme } from '../themes';

//pageProps is an object with the initial props that were preloaded 
//for your page by one of our data fetching methods, otherwise it's an empty object.
 
function MyApp({ Component, pageProps } : AppProps)  {
  return (
    <SnackbarProvider maxSnack={ 3 }>
       <EntriesProvider>
      <UIProvider>
      <ThemeProvider theme={ darkTheme }>
        <CssBaseline />
        <Component {...pageProps} />
      </ThemeProvider>
    </UIProvider>
    </EntriesProvider>
    </SnackbarProvider>
  )
}

export default MyApp