import type { NextApiRequest, NextApiResponse } from 'next'
import { Entry, IEntry } from "../../../models"
import { db } from '../../../database'
import mongoose from 'mongoose'

type Data =
    | { message: string }
    | IEntry


export default function handler(req: NextApiRequest, res: NextApiResponse<Data>) {

    const { id } = req.query

    if (!mongoose.isValidObjectId(id)) {
        return res.status(400).json({ message: 'El id no es valido' + id })
    }

    switch (req.method) {
        case 'PUT':
            return updateEntry(req, res)

        case 'GET':
            return getEntry(req, res)

        default:
            res.status(400).json({ message: 'Metodo no existe' })
    }


}

const getEntry= async(req: NextApiRequest, res: NextApiResponse<Data> ) =>{
    const { id } = req.query

    await db.connect();

    const entryInDB = await Entry.findById(id);
    await db.disconnect()
    //sirve para actualizar esatdo y la descripcion
    if (!entryInDB) {
        return res.status(400).json({ message: 'No hay entrada con ese ID:' + id })
    }
    return res.status(200).json(entryInDB)
}

const updateEntry = async (req: NextApiRequest, res: NextApiResponse<Data>) => {
    const { id } = req.query

    await db.connect();

    const entryToUpdate = await Entry.findById(id);
    //sirve para actualizar esatdo y la descripcion
    if (!entryToUpdate) {
        await db.disconnect()
        return res.status(400).json({ message: 'No hay entrada con ese ID:' + id })
    }
    //En este caso desdestructuro la constante. Si viene la descripcion la uso, sino uso la del entrytoupdate.
    //Same with the status. Si no viene nada, usa la info que vineen desde el  body
    const {
        description = entryToUpdate.description,
        status = entryToUpdate.status
    } = req.body

    try {
        // como argumento tambien podemos pasarle {runValidators: true, new: true} para que haga las validaciones necesarias por detrás 
        const updatedEntry = await Entry.findByIdAndUpdate(id, { description, status })
        await db.disconnect()
        res.status(200).json(updatedEntry!)
    } catch (error) {
        await db.disconnect()
        res.status(400).json({message: 'Bad request'})
    }
} 