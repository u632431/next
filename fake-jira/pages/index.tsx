import type {NextPage} from 'next'
import {Grid, Card, CardHeader} from '@mui/material'
import {Layout} from '../components/Layouts/'
import { EntryList, NewEntry } from '../components/ui'

const Home: NextPage = () =>{
    return (
        <Layout title='Home - FakeJira'>
            <Grid container spacing={ 2 }>
                <Grid item xs={ 12 } sm={ 4 }>
                    <Card sx={{ height: 'calc(100vh - 100px)' }}>
                        <CardHeader title="Pendientes"/>
                        <NewEntry/>
                        <EntryList status='pending'/>
                    </Card>
                </Grid>
                <Grid item xs={ 12 } sm={ 4 }>
                    <Card sx={{ height: 'calc(100vh - 100px)' }}>
                        <CardHeader title="En curso"/>
                        <EntryList status='in-progress'/>
                    </Card>
                </Grid>
                <Grid item xs={ 12 } sm={ 4 }>
                    <Card sx={{ height: 'calc(100vh - 100px)' }}>
                        <CardHeader title="Finalizadas"/>
                        <EntryList status='finished'/>
                    </Card>
                </Grid>
             
            </Grid>
        </Layout>
    )
}

export default Home