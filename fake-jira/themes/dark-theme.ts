import {createTheme} from '@mui/material'
import {grey, red} from '@mui/material/colors'

export const darkTheme = createTheme({
    palette:{
        mode:'dark',
        background:{ 
            default: grey[700]
        },
        primary:{
            main: '#4a148c'
        },
        secondary:{
            main: '#19857b'
        },
        error:{
            main: red.A400
        }
    },
    components:{
        MuiAppBar:{
            defaultProps:{},
            styleOverrides:{
                root:{
                    backgroundColor: '#4A148C'
                }
            }
        }
    }
})
