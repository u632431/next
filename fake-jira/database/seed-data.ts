//para llenar la base de datos con un solo comando. Info que voy a meter de forma manual

interface SeedData {
    entries: SeedEntry[]
}

interface SeedEntry {
    description: string
    status: string
    createdAt: number
}

export const seedData: SeedData = {
    entries: [
        {
            description: "Pending:Proident dolor duis elit sunt qui dolor laborum veniam ea laboris qui consequat.",
            status: "pending",
            createdAt: Date.now()
        },
        {
            description: "In-progress: Proident dolor duis elit sunt qui dolor laborum veniam ea laboris qui consequat.",
            status: "in-progress",
            createdAt: Date.now() - 1000000
        },
        {
            description: "Finished:Proident dolor duis elit sunt qui dolor laborum veniam ea laboris qui consequat.",
            status: "finished",
            createdAt: Date.now() - 100000
        },
    ]
}

